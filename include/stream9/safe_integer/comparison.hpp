#ifndef STREAM9_SAFE_INTEGER_COMPARISON_HPP
#define STREAM9_SAFE_INTEGER_COMPARISON_HPP

#include <stream9/safe_integer/fwd.hpp>
#include <stream9/safe_integer/conversion.hpp>
#include <stream9/safe_integer/type_traits.hpp>

#include <type_traits>

namespace stream9::safe_integers {

template<typename T, typename U>
    requires is_integral_v<T, U>
constexpr bool equal(T const lhs, U const rhs)
{
    using common_t = arithmetic_common_t<T, U>;

    if constexpr (std::is_signed_v<T> && std::is_unsigned_v<U>) {
        if (lhs < 0) {
            return false;
        }
        else {
            auto const lhs_ = static_cast<common_t>(lhs);

            return lhs_ == rhs;
        }
    }
    else if constexpr (std::is_unsigned_v<T> && std::is_signed_v<U>){
        if (rhs < 0) {
            return false;
        }
        else {
            auto const rhs_ = static_cast<common_t>(rhs);

            return lhs == rhs_;
        }
    }
    else {
        return lhs == rhs;
    }
}

template<typename T, typename U>
    requires is_integer_v<T, U>
constexpr bool not_equal(T const lhs, U const rhs)
{
    return !equal(lhs, rhs);
}

template<typename T, typename U>
    requires is_integral_v<T, U>
constexpr bool less(T const lhs, U const rhs)
{
    using common_t = arithmetic_common_t<T, U>;

    if constexpr (std::is_signed_v<T> && std::is_unsigned_v<U>) {
        if (lhs < 0) {
            return true;
        }
        else {
            auto const lhs_ = static_cast<common_t>(lhs);
            return lhs_ < rhs;
        }
    }
    else if constexpr (std::is_unsigned_v<T> && std::is_signed_v<U>){
        if (rhs < 0) {
            return false;
        }
        else {
            auto const rhs_ = static_cast<common_t>(rhs);
            return lhs < rhs_;
        }
    }
    else {
        return lhs < rhs;
    }
}

template<typename T, typename U>
    requires is_integer_v<T, U>
constexpr bool greater(T const lhs, U const rhs)
{
    return less(rhs, lhs);
}

template<typename T, typename U>
    requires is_integer_v<T, U>
constexpr bool less_equal(T const lhs, U const rhs)
{
    return !greater(lhs, rhs);
}

template<typename T, typename U>
    requires is_integer_v<T, U>
constexpr bool greater_equal(T const lhs, U const rhs)
{
    return !less(lhs, rhs);
}

template<typename T, typename U>
    requires is_integer_v<T, U>
constexpr bool between(T const val, U const min, U const max)
{
    return greater_equal(val, min) && less_equal(val, max);
}

// for safe_integer

namespace detail {

template<typename T, typename U>
bool constexpr is_overlapped_v =
    less_equal(T::min(), U::min()) ?
          greater_equal(T::max(), U::min())
        : less_equal(T::min(), U::max());

} // namespace detail

template<typename T, typename U>
    requires is_safe_integer_v<T, U>
constexpr bool
equal([[maybe_unused]] T const lhs, [[maybe_unused]] U const rhs)
{
    if constexpr (!detail::is_overlapped_v<T, U>) {
        return false;
    }
    else if constexpr (std::is_signed_v<T> && std::is_unsigned_v<U>
                    && greater_equal(T::min(), 0))
    {
        using unsigned_T = std::make_unsigned_t<typename T::value_type>;

        return equal(static_cast<unsigned_T>(lhs.value()), rhs.value());
    }
    else if constexpr (std::is_unsigned_v<T> && std::is_signed_v<U>
                                             && greater_equal(U::min(), 0))
    {
        using unsigned_U = std::make_unsigned_t<typename U::value_type>;

        return equal(lhs.value(), static_cast<unsigned_U>(rhs.value()));
    }
    else {
        return equal(lhs.value(), rhs.value());
    }
}

template<typename T, typename U>
    requires is_safe_integer_v<T>
          && is_integral_v<U>
constexpr bool
equal(T const lhs, U const rhs)
{
    return equal(lhs, safe_integer { rhs });
}

template<typename T, typename U>
    requires is_integral_v<T>
          && is_safe_integer_v<U>
constexpr bool
equal(T const lhs, U const rhs)
{
    return equal(safe_integer { lhs }, rhs);
}

template<typename T, typename U>
    requires is_safe_integer_v<T, U>
constexpr bool
less([[maybe_unused]] T const lhs, [[maybe_unused]] U const rhs)
{
    if constexpr (less(T::max(), U::min())) {
        return true;
    }
    else if constexpr (less(U::max(), T::min())) {
        return false;
    }
    else if constexpr (std::is_signed_v<T> && std::is_unsigned_v<U>
                    && greater_equal(T::min(), 0))
    {
        using unsigned_T = std::make_unsigned_t<typename T::value_type>;

        return less(static_cast<unsigned_T>(lhs.value()), rhs.value());
    }
    else if constexpr (std::is_unsigned_v<T> && std::is_signed_v<U>
                                             && greater_equal(U::min(), 0))
    {
        using unsigned_U = std::make_unsigned_t<typename U::value_type>;

        return less(lhs.value(), static_cast<unsigned_U>(rhs.value()));
    }
    else {
        return less(lhs.value(), rhs.value());
    }
}

template<typename T, typename U>
    requires is_safe_integer_v<T>
          && is_integral_v<U>
constexpr bool
less(T const lhs, U const rhs)
{
    return less(lhs, safe_integer { rhs });
}

template<typename T, typename U>
    requires is_integral_v<T>
          && is_safe_integer_v<U>
constexpr bool
less(T const lhs, U const rhs)
{
    return less(safe_integer { lhs }, rhs);
}

} // namespace stream9::safe_integers

#endif // STREAM9_SAFE_INTEGER_COMPARISON_HPP
