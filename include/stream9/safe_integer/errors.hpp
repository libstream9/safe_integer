#ifndef STREAM9_SAFE_INTEGER_ERRORS_HPP
#define STREAM9_SAFE_INTEGER_ERRORS_HPP

#include <stream9/safe_integer/fwd.hpp>

#include <stream9/safe_integer/arithmetic_outcome.hpp>
#include <stream9/safe_integer/any_integer.hpp>
#include <stream9/safe_integer/type_traits.hpp>

#include <iosfwd>
#include <optional>
#include <stdexcept>

namespace stream9::safe_integers {

enum class operation {
    construction,
    assignment,
    addition,
    subtraction,
    multiplication,
    division,
    modulo,
    negation,
    conversion,
};

std::ostream& operator<<(std::ostream&, operation);

class overflow_error : public std::overflow_error
{
public:
    template<typename T>
        requires is_integer_v<T>
    overflow_error(operation op, T const& op1)
        : std::overflow_error { "safe_integer: overflow" }
        , m_operation { op }
        , m_operand1 { op1 }
    {}

    template<typename T, typename U>
        requires is_integer_v<T, U>
    overflow_error(operation op, T const& op1, U const& op2)
        : std::overflow_error { "safe_integer: overflow" }
        , m_operation { op }
        , m_operand1 { op1 }
        , m_operand2 { op2 }
    {}

    enum operation operation() const { return m_operation; }
    any_integer const& operand1() const { return m_operand1; }
    std::optional<any_integer> const& operand2() const { return m_operand2; }

private:
    enum operation m_operation;
    any_integer m_operand1;
    std::optional<any_integer> m_operand2;
};

std::ostream& operator<<(std::ostream&, overflow_error const&);

class underflow_error : public std::underflow_error
{
public:
    template<typename T>
        requires is_integer_v<T>
    underflow_error(operation op, T const& op1)
        : std::underflow_error { "safe_integer: underflow" }
        , m_operation { op }
        , m_operand1 { op1 }
    {}

    template<typename T, typename U>
        requires is_integer_v<T, U>
    underflow_error(operation op, T const& op1, U const& op2)
        : std::underflow_error { "safe_integer: underflow" }
        , m_operation { op }
        , m_operand1 { op1 }
        , m_operand2 { op2 }
    {}

    enum operation operation() const { return m_operation; }
    any_integer const& operand1() const { return m_operand1; }
    std::optional<any_integer> const& operand2() const { return m_operand2; }

private:
    enum operation m_operation;
    any_integer m_operand1;
    std::optional<any_integer> m_operand2;
};

std::ostream& operator<<(std::ostream&, underflow_error const&);

class divided_by_zero_error : public std::runtime_error
{
public:
    template<typename T>
        requires is_integer_v<T>
    divided_by_zero_error(T const op)
        : std::runtime_error { "safe_integer: divided by zero" }
        , m_operand { op }
    {}

    any_integer const& operand() const { return m_operand; }

private:
    any_integer m_operand;
};

std::ostream& operator<<(std::ostream&, divided_by_zero_error const&);

template<typename T>
        requires is_integral_v<T>
constexpr void handle_construction_error(T const value,
                               arithmetic_errc const error_code)
{
    switch (error_code) {
    case arithmetic_errc::overflow:
        throw overflow_error { operation::construction, value };
    case arithmetic_errc::underflow:
        throw underflow_error { operation::construction, value };
    default:
        assert(false);
    }
}

template<typename T, typename U>
        requires is_integer_v<T, U>
constexpr typename T::value_type
handle_assignment_error(
        T const dest,
        U const src,
        arithmetic_errc const error_code)
{
    switch (error_code) {
    case arithmetic_errc::overflow:
        throw overflow_error {
            operation::assignment, dest, src };
    case arithmetic_errc::underflow:
        throw underflow_error {
            operation::assignment, dest, src };
    default:
        assert(false);
    }

    return {};
}

template<typename U, typename T>
        requires is_integral_v<T, U>
constexpr U
handle_conversion_error(
        T const src,
        arithmetic_errc const error_code)
{

    switch (error_code) {
    case arithmetic_errc::overflow:
        throw overflow_error { operation::conversion, src, U() };
    case arithmetic_errc::underflow:
        throw underflow_error { operation::conversion, src, U() };
    default:
        assert(false);
    }

    return {};
}

template<typename T, typename U>
        requires is_integer_v<T, U>
constexpr U
handle_arithmetic_error(
        T const operand,
        operation const op,
        arithmetic_outcome<U> const outcome)
{
    switch (outcome.error()) {
    case arithmetic_errc::overflow:
        throw overflow_error { op, operand };
    case arithmetic_errc::underflow:
        throw underflow_error { op, operand };
    default:
        assert(false);
    }

    return {};
}

template<typename Lhs, typename Rhs, typename Result>
        requires is_integer_v<Lhs, Rhs, Result>
constexpr Result
handle_arithmetic_error(
    Lhs const lhs,
    Rhs const rhs,
    operation const op,
    arithmetic_outcome<Result> const outcome)
{
    switch (outcome.error()) {
    case arithmetic_errc::overflow:
        throw overflow_error { op, lhs, rhs };
    case arithmetic_errc::underflow:
        throw underflow_error { op, lhs, rhs };
    case arithmetic_errc::divide_by_zero:
        throw divided_by_zero_error { lhs };
    default:
        assert(false);
    }

    return {};
}

} // namespace stream9::safe_integers

#endif // STREAM9_SAFE_INTEGER_ERRORS_HPP
