#ifndef STREAM9_SAFE_INTEGER_ANY_INTEGER_HPP
#define STREAM9_SAFE_INTEGER_ANY_INTEGER_HPP

#include <stream9/safe_integer/fwd.hpp>
#include <stream9/safe_integer/type_traits.hpp>

#include <iosfwd>
#include <string>
#include <variant>

namespace stream9::safe_integers {

class any_safe_integer
{
public:
    using underlying_type = std::variant<
        short int, unsigned short int,
        int, unsigned int,
        long int, unsigned long int,
        long long int, unsigned long long int
    >;

public:
    template<typename T, T Min, T Max>
    any_safe_integer(safe_integer<T, Min, Max> const value)
        : m_value { value.value() }
        , m_min { Min }
        , m_max { Max }
    {}

    // accessor
    underlying_type const& value() const noexcept { return m_value; }
    underlying_type const& minimum() const noexcept { return m_min; }
    underlying_type const& maximum() const noexcept { return m_max; }

    // query
    std::string type_name() const;

private:
    underlying_type m_value;
    underlying_type m_min;
    underlying_type m_max;
};

std::ostream& operator<<(std::ostream&, any_safe_integer const&);

class any_integer
{
public:
    using underlying_type = std::variant<
        short int, unsigned short int,
        int, unsigned int,
        long int, unsigned long int,
        long long int, unsigned long long int,
        any_safe_integer
    >;

public:
    template<typename T>
        requires is_integral_v<T>
    explicit any_integer(T const v)
        : m_value { v }
    {}

    template<typename T>
        requires is_safe_integer_v<T>
    explicit any_integer(T const v)
        : m_value { any_safe_integer { v } }
    {}

    // accessor
    underlying_type const& value() const noexcept { return m_value; }

private:
    underlying_type m_value;
};

std::ostream& operator<<(std::ostream&, any_integer const&);

} // namespace stream9::safe_integers

#endif // STREAM9_SAFE_INTEGER_ANY_INTEGER_HPP
