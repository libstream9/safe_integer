#ifndef STREAM9_SAFE_INTEGER_FWD_HPP
#define STREAM9_SAFE_INTEGER_FWD_HPP

#include <limits>
#include <type_traits>

namespace stream9::safe_integers {

template<typename T,
         T Minimum = std::numeric_limits<T>::min(),
         T Maximum = std::numeric_limits<T>::max()>
    requires requires {
        std::is_integral_v<T>;
        Minimum <= Maximum;
    }
class safe_integer;

} // namespace stream9::safe_integers

#endif // STREAM9_SAFE_INTEGER_FWD_HPP
