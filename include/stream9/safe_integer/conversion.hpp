#ifndef STREAM9_SAFE_INTEGER_CONVERSION_HPP
#define STREAM9_SAFE_INTEGER_CONVERSION_HPP

#include <stream9/safe_integer/type_traits.hpp>

#include <type_traits>

namespace stream9::safe_integers {

// type for integral promotion

template<typename T>
    requires is_integral_v<T>
using integral_promote_t = decltype(+T());

template<typename T>
    requires is_integral_v<T>
constexpr auto integral_promotion(T const value) noexcept
{
    using result_t = integral_promote_t<T>;

    return static_cast<result_t>(value);
}

// type for usual arithmetic conversion

template<typename T, typename U>
    requires is_integral_v<T, U>
using arithmetic_common_t = decltype(T() + U());

} // namespace stream9::safe_integers

#endif // STREAM9_SAFE_INTEGER_CONVERSION_HPP
