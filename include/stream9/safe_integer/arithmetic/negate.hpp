#ifndef STREAM9_SAFE_INTEGER_ARITHMETIC_NEGATE_HPP
#define STREAM9_SAFE_INTEGER_ARITHMETIC_NEGATE_HPP

#include <stream9/safe_integer/fwd.hpp>
#include <stream9/safe_integer/type_traits.hpp>
#include <stream9/safe_integer/arithmetic/safe_negate.hpp>
#include <stream9/safe_integer/arithmetic_outcome.hpp>
#include <stream9/safe_integer/conversion.hpp>

#include <utility>
#include <limits>

namespace stream9::safe_integers {

template<typename T>
class negate_type;


template<typename T>
    requires is_safe_integer_v<T>
constexpr auto
negate(T const value)
{
    using result_t = typename negate_type<T>::type;
    using value_t = typename result_t::value_type;

    auto make_option = [&] {
        auto result = arithmetic_option::none;

        if constexpr (result_t::is_left_bounded()) {
            result |= arithmetic_option::dont_check_underflow;
        }

        if constexpr (result_t::is_right_bounded()) {
            result |= arithmetic_option::dont_check_overflow;
        }

        return result;
    };

    auto const oc =
        safe_negate<value_t, make_option()>(value.value());
    if (oc) {
        return arithmetic_outcome<result_t> { *oc };
    }
    else {
        return arithmetic_outcome<result_t> { oc.error() };
    }
}


template<typename T>
class negate_type
{
private:
    using T_t = typename T::value_type;
    using value_t = typename decltype(safe_negate(T_t()))::value_type;

    static constexpr auto boundary()
    {
        auto const neg_min = safe_negate<value_t>(T::min());
        auto const neg_max = safe_negate<value_t>(T::max());

        if (neg_min < neg_max) {
            return std::make_pair(neg_min, neg_max);
        }
        else {
            return std::make_pair(neg_max, neg_min);
        }
    }

    static auto constexpr m_boundary = boundary();

public:
    using type = safe_integer<
        value_t,
        m_boundary.first.value_or(std::numeric_limits<value_t>::min()),
        m_boundary.second.value_or(std::numeric_limits<value_t>::max())
    >;
};

} // namespace stream9::safe_integers

#endif // STREAM9_SAFE_INTEGER_ARITHMETIC_NEGATE_HPP
