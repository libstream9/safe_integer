#ifndef STREAM9_SAFE_INTEGER_ARITHMETIC_MODULO_HPP
#define STREAM9_SAFE_INTEGER_ARITHMETIC_MODULO_HPP

#include <stream9/safe_integer/arithmetic/safe_convert.hpp>
#include <stream9/safe_integer/arithmetic/safe_modulo.hpp>
#include <stream9/safe_integer/arithmetic/safe_negate.hpp>
#include <stream9/safe_integer/arithmetic_outcome.hpp>
#include <stream9/safe_integer/conversion.hpp>
#include <stream9/safe_integer/fwd.hpp>
#include <stream9/safe_integer/type_traits.hpp>
#include <stream9/safe_integer/utility.hpp>

#include <type_traits>
#include <limits>

namespace stream9::safe_integers {

template<typename T, typename U>
class modulo_type;


template<typename T, typename U>
    requires is_safe_integer_v<T, U>
constexpr auto // arithmetic_outcome<safe_integer>
modulo(T const lhs, U const rhs)
{
    using result_t = typename modulo_type<T, U>::type;
    using value_t = typename result_t::value_type;

    auto make_option = [&] {
        auto result = arithmetic_option::none;

        if constexpr (T::is_left_bounded()) {
            result |= arithmetic_option::dont_check_underflow;
        }

        if constexpr (T::is_right_bounded()) {
            result |= arithmetic_option::dont_check_overflow;
        }

        if constexpr (less(0, U::min()) || less(U::max(), 0)) {
            result |= arithmetic_option::dont_check_zero_division;
        }

        return result;
    };

    auto const rv =
            safe_modulo<value_t, make_option()>(lhs.value(), rhs.value());
    if (rv) {
        return arithmetic_outcome<result_t> { rv.value() };
    }
    else {
        return arithmetic_outcome<result_t> { rv.error() };
    }
}


template<typename T, typename U>
class modulo_type
{
private:
    using T_t = typename T::value_type;
    using U_t = typename U::value_type;
    using value_t = arithmetic_common_t<T_t, U_t>;

    static_assert(!(U::min() == 0 && U::max() == 0), "Zero division guaranteed");

    template<typename T1, typename T2>
    static constexpr auto get_bound(T1 const lhs, T2 const rhs)
    {
        if (less(safe_abs(lhs), rhs)) {
            return safe_convert<value_t>(lhs);
        }
        else { // rhs <= abs(lhs)
            if (greater_equal(lhs, 0)) {
                return safe_convert<value_t>(rhs - 1);
            } else {
                return safe_negate<value_t>(rhs - 1);
            }
        }
    }

    static auto constexpr boundary()
    {
        auto const t_min = integral_promotion(T::min());
        auto const t_max = integral_promotion(T::max());
        auto const u_min = integral_promotion(U::min());
        auto const u_max = integral_promotion(U::max());

        auto const u_abs = utility::max(safe_abs(u_min), safe_abs(u_max));

        return std::make_pair(
            get_bound(less(0, t_min) ? 0 : t_min, u_abs),
            get_bound(less(t_max, 0) ? 0 : t_max, u_abs)
        );
    }

    static auto constexpr m_boundary = boundary();

public:
    using type = safe_integer<
        value_t,
        m_boundary.first.value_or(std::numeric_limits<value_t>::min()),
        m_boundary.second.value_or(std::numeric_limits<value_t>::max())
    >;
};

} // namespace stream9::safe_integers

#endif // STREAM9_SAFE_INTEGER_ARITHMETIC_MODULO_HPP
