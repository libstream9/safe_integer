#ifndef STREAM9_SAFE_INTEGER_ARITHMETIC_SAFE_MULTIPLY_HPP
#define STREAM9_SAFE_INTEGER_ARITHMETIC_SAFE_MULTIPLY_HPP

#include <stream9/safe_integer/arithmetic_option.hpp>
#include <stream9/safe_integer/arithmetic_outcome.hpp>
#include <stream9/safe_integer/comparison.hpp>
#include <stream9/safe_integer/conversion.hpp>
#include <stream9/safe_integer/type_traits.hpp>

namespace stream9::safe_integers {

template<typename V, arithmetic_option options,
         typename T, typename U>
    requires is_integral_v<T, U, V>
          && (!check_overflow(options) && !check_underflow(options))
constexpr arithmetic_outcome<V>
safe_multiply(T const lhs, U const rhs)
{
    if constexpr (is_same_signedness_v<T, U>) {
        return lhs * rhs;
    }
    else {
        auto const lhs_ = integral_promotion(lhs);
        auto const rhs_ = integral_promotion(rhs);

        V result {};

        if (__builtin_mul_overflow(lhs_, rhs_, &result)) {
            assert(false);
        }

        return result;
    }
}

template<typename V, arithmetic_option options = arithmetic_option::none,
         typename T, typename U>
    requires is_integral_v<T, U, V>
          && (check_overflow(options) || check_underflow(options))
constexpr arithmetic_outcome<V>
safe_multiply(T const lhs_, U const rhs_)
{
    auto const lhs = integral_promotion(lhs_);
    auto const rhs = integral_promotion(rhs_);

    V result {};

    if (__builtin_mul_overflow(lhs, rhs, &result)) {
        auto is_result_positive = [&] {
            return !((lhs < 0) ^ (rhs < 0));
        };

        if (is_result_positive()) {
            if constexpr (check_overflow(options)) {
                return arithmetic_errc::overflow;
            }
        }
        else {
            if constexpr (check_underflow(options)) {
                return arithmetic_errc::underflow;
            }
        }
    }

    return result;
}

} // namespace stream9::safe_integers

#endif // STREAM9_SAFE_INTEGER_ARITHMETIC_SAFE_MULTIPLY_HPP
