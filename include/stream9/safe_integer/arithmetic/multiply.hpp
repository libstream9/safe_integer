#ifndef STREAM9_SAFE_INTEGER_ARITHMETIC_MULTIPLY_HPP
#define STREAM9_SAFE_INTEGER_ARITHMETIC_MULTIPLY_HPP

#include <stream9/safe_integer/fwd.hpp>
#include <stream9/safe_integer/arithmetic/safe_multiply.hpp>
#include <stream9/safe_integer/arithmetic_outcome.hpp>
#include <stream9/safe_integer/type_traits.hpp>
#include <stream9/safe_integer/conversion.hpp>
#include <stream9/safe_integer/utility.hpp>

#include <type_traits>
#include <limits>

namespace stream9::safe_integers {

template<typename T, typename U>
class multiply_type;


template<typename T, typename U>
    requires is_safe_integer_v<T, U>
constexpr auto // arithmetic_outcome<safe_integer>
multiply(T const lhs, U const rhs)
{
    using result_t = typename multiply_type<T, U>::type;
    using value_t = typename result_t::value_type;

    auto make_option = [&] {
        auto result = arithmetic_option::none;

        if constexpr (T::is_left_bounded()) {
            result |= arithmetic_option::dont_check_underflow;
        }

        if constexpr (T::is_right_bounded()) {
            result |= arithmetic_option::dont_check_overflow;
        }

        return result;
    };

    auto const rv =
            safe_multiply<value_t, make_option()>(lhs.value(), rhs.value());
    if (rv) {
        return arithmetic_outcome<result_t> { rv.value() };
    }
    else {
        return arithmetic_outcome<result_t> { rv.error() };
    }
}


template<typename T, typename U>
class multiply_type
{
private:
    using T_t = typename T::value_type;
    using U_t = typename U::value_type;
    using value_t = arithmetic_common_t<T_t, U_t>;

    template<typename T1, typename T2>
    static auto constexpr multiply(T1 const lhs, T2 const rhs)
    {
        return safe_multiply<value_t>(lhs, rhs);
    }

    static auto constexpr m_mul1 = multiply(T::min(), U::min());
    static auto constexpr m_mul2 = multiply(T::min(), U::max());
    static auto constexpr m_mul3 = multiply(T::max(), U::min());
    static auto constexpr m_mul4 = multiply(T::max(), U::max());

    static auto constexpr m_min = utility::min(m_mul1, m_mul2, m_mul3, m_mul4);
    static auto constexpr m_max = utility::max(m_mul1, m_mul2, m_mul3, m_mul4);

public:
    using type = safe_integer<
        value_t,
        m_min.value_or(std::numeric_limits<value_t>::min()),
        m_max.value_or(std::numeric_limits<value_t>::max())
    >;
};

} // namespace stream9::safe_integers

#endif // STREAM9_SAFE_INTEGER_ARITHMETIC_MULTIPLY_HPP
