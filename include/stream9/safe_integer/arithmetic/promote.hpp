#ifndef STREAM9_SAFE_INTEGER_ARITHMETIC_PROMOTE_HPP
#define STREAM9_SAFE_INTEGER_ARITHMETIC_PROMOTE_HPP

#include <stream9/safe_integer/fwd.hpp>
#include <stream9/safe_integer/type_traits.hpp>
#include <stream9/safe_integer/conversion.hpp>

#include <type_traits>

namespace stream9::safe_integers {

template<typename T>
class promote_type;


template<typename T>
    requires is_safe_integer_v<T>
constexpr auto
promote(T const value) noexcept
{
    using result_t = typename promote_type<T>::type;

    return result_t { integral_promotion(value.value()) };
}


template<typename T>
class promote_type
{
private:
    using T_t = typename T::value_type;
    using value_t = integral_promote_t<T_t>;

public:
    using type = safe_integer<
        value_t,
        integral_promotion(T::min()),
        integral_promotion(T::max())
    >;
};

} // namespace stream9::safe_integers

#endif // STREAM9_SAFE_INTEGER_ARITHMETIC_PROMOTE_HPP
