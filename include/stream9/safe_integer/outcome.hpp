#ifndef STREAM9_SAFE_INTEGER_OUTCOME_HPP
#define STREAM9_SAFE_INTEGER_OUTCOME_HPP

#include "comparison.hpp"

#include <cassert>
#include <iosfwd>

namespace stream9::safe_integers {

template<typename T, typename E>
    requires requires {
        E::ok;
    }
class outcome
{
public:
    constexpr outcome(T const value) noexcept : m_value { value } {}
    constexpr outcome(E const errc) noexcept : m_errc { errc } {}

    constexpr T value() const noexcept { assert(has_value()); return m_value; }
    constexpr E error() const noexcept { return m_errc; }

    constexpr T value_or(T const v) const noexcept
    {
        return has_value() ? value() : v;
    }

    constexpr bool has_value() const noexcept { return error() == E::ok; }

    constexpr T operator*() const noexcept { return value(); }
    constexpr operator bool() const noexcept { return has_value(); }

    template<typename U>
    bool operator==(outcome<U, E> const other) const noexcept
    {
        if (has_value()) {
            return other.has_value() ? equal(value(), other.value()) : false;
        }
        else {
            return other.has_value() ? false : error() == other.error();
        }
    }

    template<typename U>
    bool operator!=(outcome<U, E> const other) const noexcept
    {
        return !operator==(other);
    }

private:
    T m_value {};
    E m_errc = E::ok;
};

template<typename T, typename E>
std::ostream&
operator<<(std::ostream& os, outcome<T, E> const& oc)
{
    if (oc.has_value()) {
        os << oc.value();
    }
    else {
        os << oc.error();
    }

    return os;
}

} // namespace stream9::safe_integers

#endif // STREAM9_SAFE_INTEGER_OUTCOME_HPP
