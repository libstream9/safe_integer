#ifndef SAFE_INEGER_ARITHMETIC_OPTION_HPP
#define SAFE_INEGER_ARITHMETIC_OPTION_HPP

#include <iosfwd>
#include <type_traits>

namespace stream9::safe_integers {

enum class arithmetic_option {
    none = 0,
    dont_check_overflow = 1,
    dont_check_underflow = 2,
    dont_check_zero_division = 4
};

inline constexpr arithmetic_option
operator&(arithmetic_option const lhs, arithmetic_option const rhs)
{
    using value_t = std::underlying_type_t<arithmetic_option>;

    return static_cast<arithmetic_option>(
            static_cast<value_t>(lhs) & static_cast<value_t>(rhs));
}

inline constexpr arithmetic_option&
operator&=(arithmetic_option& lhs, arithmetic_option const rhs)
{
    return lhs = lhs & rhs;
}

inline constexpr arithmetic_option
operator|(arithmetic_option const lhs, arithmetic_option const rhs)
{
    using value_t = std::underlying_type_t<arithmetic_option>;

    return static_cast<arithmetic_option>(
            static_cast<value_t>(lhs) | static_cast<value_t>(rhs));
}

inline constexpr arithmetic_option&
operator|=(arithmetic_option& lhs, arithmetic_option const rhs)
{
    return lhs = lhs | rhs;
}

inline constexpr bool
test_option(arithmetic_option const options, arithmetic_option const flag)
{
    return (options & flag) == flag;
}

inline constexpr bool
check_overflow(arithmetic_option const options)
{
    return !test_option(options, arithmetic_option::dont_check_overflow);
}

inline constexpr bool
check_underflow(arithmetic_option const options)
{
    return !test_option(options, arithmetic_option::dont_check_underflow);
}

inline constexpr bool
check_zero_division(arithmetic_option const options)
{
    return !test_option(options, arithmetic_option::dont_check_zero_division);
}

std::ostream& operator<<(std::ostream&, arithmetic_option);

} // namespace stream9::safe_integers

#endif // SAFE_INEGER_ARITHMETIC_OPTION_HPP
