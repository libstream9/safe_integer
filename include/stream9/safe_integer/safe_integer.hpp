#ifndef STREAM9_SAFE_INTEGER_SAFE_INTEGER_HPP
#define STREAM9_SAFE_INTEGER_SAFE_INTEGER_HPP

#include <stream9/safe_integer/fwd.hpp>
#include <stream9/safe_integer/comparison.hpp>
#include <stream9/safe_integer/outcome.hpp>
#include <stream9/safe_integer/arithmetic.hpp>
#include <stream9/safe_integer/type_traits.hpp>
#include <stream9/safe_integer/conversion.hpp>
#include <stream9/safe_integer/errors.hpp>
#include <stream9/safe_integer/arithmetic/safe_convert.hpp>

#include <iosfwd>
#include <type_traits>
#include <limits>

namespace stream9::safe_integers {

template<typename T>
constexpr auto make_safe_integer(T);

template<auto Min, auto Max, typename T>
constexpr auto make_safe_integer(T);

// safe_integer

template<typename T,
         T Minimum/*= std::numeric_limits<T>::min()*/,
         T Maximum/*= std::numeric_limits<T>::max()*/>
    requires requires {
        std::is_integral_v<T>;
        Minimum <= Maximum;
    }
class safe_integer
{
public:
    using value_type = T;

public:
    // constructors

    constexpr safe_integer() noexcept
    {
        if constexpr (T() < min() || T() > max()) {
            m_value = min();
        }
    }

    safe_integer(safe_integer const&) = default;

    constexpr safe_integer(T const v)
        : m_value { v }
    {
        if (is_overflow(m_value)) {
            handle_construction_error(v, arithmetic_errc::overflow);
        }
        else if (is_underflow(m_value)) {
            handle_construction_error(v, arithmetic_errc::underflow);
        }
    }

    template<typename U>
        requires is_safe_integer_v<U>
    constexpr safe_integer(U const& other)
    {
        static_assert(is_overlapped<U>());

        if (less(U::min(), min()) && less(other.value(), min())) {
            handle_construction_error(
                    other.value(), arithmetic_errc::underflow);
        }
        else if (greater(U::max(), max()) && greater(other.value(), max())) {
            handle_construction_error(
                    other.value(), arithmetic_errc::overflow);
        }
        else {
            m_value = static_cast<value_type>(other.value());
        }
    }

    template<typename U>
        requires is_integral_v<U>
    constexpr safe_integer(U const v)
        : safe_integer { safe_integer<U> { v } }
    {}

    // assignment

    safe_integer& operator=(safe_integer const&) = default;

    template<typename U>
        requires is_safe_integer_v<U>
    safe_integer& operator=(U const& other)
    {
        static_assert(is_overlapped<U>());

        if (less(U::min(), min()) && less(other.value(), min())) {
            m_value = handle_assignment_error(
                    *this, other, arithmetic_errc::underflow);
        }
        else if (greater(U::max(), max()) && greater(other.value(), max())) {
            m_value = handle_assignment_error(
                    *this, other, arithmetic_errc::overflow);
        }
        else {
            m_value = static_cast<value_type>(other.value());
        }

        return *this;
    }

    template<typename U>
        requires is_integral_v<U>
    safe_integer& operator=(U const v)
    {
        if (is_underflow(v)) {
            m_value = handle_assignment_error(
                    *this, v, arithmetic_errc::underflow);
        }
        else if (is_overflow(v)) {
            m_value = handle_assignment_error(
                    *this, v, arithmetic_errc::overflow);
        }
        else {
            m_value = static_cast<value_type>(v);
        }

        return *this;
    }

    // accessor

    constexpr T value() const noexcept { return m_value; }

    static constexpr T min() noexcept { return Minimum; }
    static constexpr T max() noexcept { return Maximum; }

    static constexpr bool is_left_bounded() noexcept
    {
        return min() != std::numeric_limits<value_type>::min();
    }

    static constexpr bool is_right_bounded() noexcept
    {
        return max() != std::numeric_limits<value_type>::max();
    }

    static constexpr bool is_bounded() noexcept
    {
        return is_left_bounded() && is_right_bounded();
    }

    // implicit converters

    constexpr operator T() const noexcept
    {
        return m_value;
    }

    template<typename U>
        requires is_integral_v<U>
    constexpr operator U() const
    {
        auto const oc = safe_convert<U>(m_value);
        if (!oc) {
            return handle_conversion_error<U>(m_value, oc.error());
        }
        else {
            return *oc;
        }
    }

    // comparison
    template<typename U>
        requires is_safe_integer_v<U> || is_integral_v<U>
    constexpr bool operator==(U const other) const noexcept
    {
        return equal(*this, other);
    }

    template<typename U>
        requires is_safe_integer_v<U> || is_integral_v<U>
    constexpr bool operator!=(U const other) const noexcept
    {
        return not_equal(*this, other);
    }

    template<typename U>
        requires is_safe_integer_v<U> || is_integral_v<U>
    constexpr bool operator<(U const other) const noexcept
    {
        return less(*this, other);
    }

    template<typename U>
        requires is_safe_integer_v<U> || is_integral_v<U>
    constexpr bool operator>(U const other) const noexcept
    {
        return greater(*this, other);
    }

    template<typename U>
        requires is_safe_integer_v<U> || is_integral_v<U>
    constexpr bool operator<=(U const other) const noexcept
    {
        return less_equal(*this, other);
    }

    template<typename U>
        requires is_safe_integer_v<U> || is_integral_v<U>
    constexpr bool operator>=(U const other) const noexcept
    {
        return greater_equal(*this, other);
    }

    template<typename U>
        requires is_integral_v<U>
    friend constexpr bool operator==(U const lhs, safe_integer const rhs) noexcept
    {
        return equal(lhs, rhs);
    }

    template<typename U>
        requires is_integral_v<U>
    friend constexpr bool operator!=(U const lhs, safe_integer const rhs) noexcept
    {
        return not_equal(lhs, rhs);
    }

    template<typename U>
        requires is_integral_v<U>
    friend constexpr bool operator<(U const lhs, safe_integer const rhs) noexcept
    {
        return less(lhs, rhs);
    }

    template<typename U>
        requires is_integral_v<U>
    friend constexpr bool operator>(U const lhs, safe_integer const rhs) noexcept
    {
        return greater(lhs, rhs);
    }

    template<typename U>
        requires is_integral_v<U>
    friend constexpr bool operator<=(U const lhs, safe_integer const rhs) noexcept
    {
        return less_equal(lhs, rhs);
    }

    template<typename U>
        requires is_integral_v<U>
    friend constexpr bool operator>=(U const lhs, safe_integer const rhs) noexcept
    {
        return greater_equal(lhs, rhs);
    }

    // arithmetic

    constexpr auto operator+() const noexcept
    {
        return promote(*this);
    }

    constexpr auto operator-() const
    {
        auto const oc = negate(*this);
        if (!oc) {
            return handle_arithmetic_error(*this, operation::negation, oc);
        }
        else {
            return *oc;
        }
    }

    template<typename U>
        requires is_safe_integer_v<U>
    constexpr auto operator+(U const other) const
    {
        auto const oc = add(*this, other);
        if (!oc) {
            return handle_arithmetic_error(*this, other, operation::addition, oc);
        }

        return *oc;
    }

    template<typename U>
        requires is_integral_v<U>
    constexpr auto operator+(U const other) const
    {
        return operator+(safe_integer<U> { other });
    }

    template<typename U>
        requires is_integral_v<U>
    friend constexpr auto operator+(U const lhs, safe_integer const rhs)
    {
        return safe_integer<U> { lhs } + rhs;
    }

    template<typename U>
        requires is_safe_integer_v<U>
    constexpr auto operator-(U const other) const
    {
        auto const oc = subtract(*this, other);
        if (!oc) {
            return handle_arithmetic_error(*this, other, operation::subtraction, oc);
        }

        return *oc;
    }

    template<typename U>
        requires is_integral_v<U>
    constexpr auto operator-(U const other) const
    {
        return *this - safe_integer<U> { other };
    }

    template<typename U>
        requires is_integral_v<U>
    friend constexpr auto operator-(U const lhs, safe_integer const rhs)
    {
        return safe_integer<U> { lhs } - rhs;
    }

    template<typename U>
        requires is_safe_integer_v<U>
    constexpr auto operator*(U const other) const
    {
        auto const oc = multiply(*this, other);
        if (!oc) {
            return handle_arithmetic_error(*this, other, operation::multiplication, oc);
        }

        return *oc;
    }

    template<typename U>
        requires is_integral_v<U>
    constexpr auto operator*(U const other) const
    {
        return *this * safe_integer<U> { other };
    }

    template<typename U>
        requires is_integral_v<U>
    friend constexpr auto operator*(U const lhs, safe_integer const rhs)
    {
        return safe_integer<U> { lhs } * rhs;
    }

    template<typename U>
        requires is_safe_integer_v<U>
    constexpr auto operator/(U const other) const
    {
        auto const oc = divide(*this, other);
        if (!oc) {
            return handle_arithmetic_error(*this, other, operation::division, oc);
        }

        return *oc;
    }

    template<typename U>
        requires is_integral_v<U>
    constexpr auto operator/(U const other) const
    {
        return *this / safe_integer<U> { other };
    }

    template<typename U>
        requires is_integral_v<U>
    friend constexpr auto operator/(U const lhs, safe_integer const rhs)
    {
        return safe_integer<U> { lhs } / rhs;
    }

    template<typename U>
        requires is_safe_integer_v<U>
    constexpr auto operator%(U const other) const
    {
        auto const oc = modulo(*this, other);
        if (!oc) {
            return handle_arithmetic_error(*this, other, operation::modulo, oc);
        }

        return *oc;
    }

    template<typename U>
        requires is_integral_v<U>
    constexpr auto operator%(U const other) const
    {
        return *this % safe_integer<U> { other };
    }

    template<typename U>
        requires is_integral_v<U>
    friend constexpr auto operator%(U const lhs, safe_integer const rhs)
    {
        return safe_integer<U> { lhs } % rhs;
    }

    // compound assignment

    template<typename U>
        requires is_safe_integer_v<U> || is_integral_v<U>
    constexpr safe_integer& operator+=(U const other)
    {
        return *this = *this + other;
    }

    template<typename U>
        requires is_safe_integer_v<U> || is_integral_v<U>
    constexpr safe_integer& operator-=(U const other)
    {
        return *this = *this - other;
    }

    template<typename U>
        requires is_safe_integer_v<U> || is_integral_v<U>
    constexpr safe_integer& operator*=(U const other)
    {
        return *this = *this * other;
    }

    template<typename U>
        requires is_safe_integer_v<U> || is_integral_v<U>
    constexpr safe_integer& operator/=(U const other)
    {
        return *this = *this / other;
    }

    template<typename U>
        requires is_safe_integer_v<U> || is_integral_v<U>
    constexpr safe_integer& operator%=(U const other)
    {
        return *this = *this % other;
    }

    // increment / decrement

    constexpr safe_integer& operator++()
    {
        return *this += 1;
    }

    constexpr safe_integer& operator--()
    {
        return *this -= 1;
    }

    constexpr safe_integer operator++(int)
    {
        auto const result = *this;

        *this += 1;

        return result;
    }

    constexpr safe_integer operator--(int)
    {
        auto const result = *this;

        *this -= 1;

        return result;
    }

    // iostream support

    friend std::ostream& operator<<(std::ostream& os, safe_integer const v)
    {
        os << v.value();
        return os;
    }

private:
    template<typename U>
    static constexpr bool is_overlapped() noexcept
    {
        static_assert(is_safe_integer_v<U>);

        return less_equal(min(), U::min()) ?
                  greater_equal(max(), U::min())
                : less_equal(min(), U::max());
    }

    static bool constexpr is_overflow([[maybe_unused]] T const value) noexcept
    {
        if constexpr (is_right_bounded()) {
            if (value > max()) return true;
        }

        return false;
    }

    template<typename U>
    static bool constexpr is_overflow([[maybe_unused]] U const value) noexcept
    {
        static_assert(std::is_integral_v<U>);

        auto constexpr u_max = std::numeric_limits<U>::max();

        if constexpr (greater(u_max, max())) {
            return greater(value, max());
        }

        return false;
    }

    static bool constexpr is_underflow([[maybe_unused]] T const value) noexcept
    {
        if constexpr (is_left_bounded()) {
            if (value < min()) return true;
        }

        return false;
    }

    template<typename U>
    static bool constexpr is_underflow([[maybe_unused]] U const value) noexcept
    {
        static_assert(std::is_integral_v<U>);

        auto constexpr u_min = std::numeric_limits<U>::min();

        if constexpr (less(u_min, min())) {
            return less(value, min());
        }

        return false;
    }

private:
    T m_value = T {};
};

template<typename T>
    requires is_integral_v<T>
constexpr auto make_safe_integer(T const value)
{
    return safe_integer<T> { value };
}

template<auto Min, auto Max, typename T>
    requires is_integral_v<T>
constexpr auto make_safe_integer(T const value)
{
    return safe_integer<T, Min, Max> { value };
}

namespace literals {

    inline auto operator""_i(unsigned long long int const i)
    {
        return safe_integer { i };
    }

} // namespace literals

} // namespace stream9::safe_integers

namespace stream9 {

using safe_integers::safe_integer;

using safe_integers::make_safe_integer;

} // namespace stream9

namespace std {

template<typename T, auto Min, auto Max>
struct is_signed<stream9::safe_integers::safe_integer<T, Min, Max>>
    : is_signed<T> {};

template<typename T, auto Min, auto Max>
struct is_unsigned<stream9::safe_integers::safe_integer<T, Min, Max>>
    : is_unsigned<T> {};

template<typename T, auto Min, auto Max>
struct is_integral<stream9::safe_integer<T, Min, Max>>
    : std::true_type {};

template<typename T, auto Min, auto Max>
class numeric_limits<stream9::safe_integer<T, Min, Max>>
    : public numeric_limits<T>
{
public:
    static constexpr T min() noexcept { return Min; }
    static constexpr T max() noexcept { return Max; }
};

} // namespace std

#endif // STREAM9_SAFE_INTEGER_SAFE_INTEGER_HPP
